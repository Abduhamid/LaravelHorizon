<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Jobs\ExampleJob;
use App\Jobs\PostJob;
use App\Jobs\RequestJob;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class RequestController extends Controller
{
    public function index(Request $request)
    {
        $file= fopen('index.txt', 'w');
        fwrite($file,$request->getContent());
        fclose($file);
        RequestJob::dispatchNow();
//        $client=new Client();
//        $response=$client->request('GET','https://wordsapiv1.p.rapidapi.com/words/', [
//            'query'=>[
//                "random"=>"true",
//            ],
//            'headers'=>[
//                "x-rapidapi-key"=> "undefined",
//	            "x-rapidapi-host"=> "wordsapiv1.p.rapidapi.com",
//	            "useQueryString"=> true
//            ],
//        ] );
////        $response=$client->request('GET', route('store'));
//
//        return $response->getBody();
////        $response=Http::get(route('store'),[
////            'name'=>'Tom',
////            'age'=>19,
////        ]);
        return 'hello world';
    }
    
    public function sync(Request $request)
    {
        $file= fopen('index.txt', 'w');
        fwrite($file,$request->getContent());
        fclose($file);
        PostJob::dispatchSync();
        return 200;
    }
}
