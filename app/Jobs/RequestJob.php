<?php

namespace App\Jobs;

use App\Models\Request;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RequestJob implements ShouldQueue
{
    const url="http://laravelrequest/public/";
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $request;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client=new Client();
//        $response=$client->request('GET',self::url.'response', [
//            'headers'=>[
//                "x-rapidapi-key"=> "undefined",
//                "x-rapidapi-host"=> "wordsapiv1.p.rapidapi.com",
//                "useQueryString"=> true
//            ],
//            'body'=>'run job',
//        ] );
//        $client=new Client();
////        $response=$client->request('GET','https://wordsapiv1.p.rapidapi.com/words/', [
////            'query'=>[
////                "random"=>"true",
////            ],
////            'headers'=>[
////                "x-rapidapi-key"=> "undefined",
////                "x-rapidapi-host"=> "wordsapiv1.p.rapidapi.com",
////                "useQueryString"=> true
////            ],
////        ] );
        $client->request('GET', 'http://laravelrequest/public/response',['body'=>'run job']);
    }
}
