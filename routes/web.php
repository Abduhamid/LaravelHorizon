<?php

use Illuminate\Support\Facades\Route;
use App\Jobs\ExampleJob;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::get('/index', function(){
//	ExampleJob::dispatch()->delay(now()->addSecond(30));
//	return 18;
//});
Route::get('/index', 'App\Http\Controllers\RequestController@index');
Route::get('/sync', 'App\Http\Controllers\RequestController@sync')->name('sync');
